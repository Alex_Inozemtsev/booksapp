--liquibase formatted sql

--changeset AlexInozemtsev:1

insert into books
    values (1,  'Some description 1', '12 стульев'),
           (2,  'Some description 2', 'Война и мир');

insert into authors
    values (1, 'Some description 1', 'Ильф'),
           (2, 'Some description 2', 'Петров'),
           (3, 'Some description 3', 'Толстой');
insert into author_book
    values (1, 1), (2, 1), (3, 2);


