package ru.itfb.booksapp.models.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;
import ru.itfb.booksapp.models.Author;
import ru.itfb.booksapp.models.Book;

import java.io.IOException;

@JsonComponent
public class BookSerializer extends JsonSerializer<Book> {

    @Override
    public void serialize(Book book, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        String[] authorNames = book.getAuthors().stream()
                .map(Author::getName)
                .toArray(String[]::new);

                jsonGenerator.writeStartObject();

                jsonGenerator.writeNumberField("id", book.getId());
                jsonGenerator.writeStringField("name", book.getName());
                jsonGenerator.writeStringField("description", book.getDescription());

                jsonGenerator.writeArrayFieldStart("authors");
                jsonGenerator.writeArray(authorNames,0, authorNames.length);
                jsonGenerator.writeEndArray();

                jsonGenerator.writeEndObject();






    }
}
