package ru.itfb.booksapp.models.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;
import ru.itfb.booksapp.models.Author;
import ru.itfb.booksapp.models.Role;
import ru.itfb.booksapp.models.User;

import java.io.IOException;

@JsonComponent
public class UserSerializer extends JsonSerializer<User> {
    @Override
    public void serialize(User user, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        String[] roles = user.getRoles().stream()
                .map(Role::getName)
                .toArray(String[]::new);

        jsonGenerator.writeStartObject();

        jsonGenerator.writeNumberField("id", user.getId());
        jsonGenerator.writeStringField("name", user.getUsername());
        jsonGenerator.writeStringField("password", user.getPassword());

        jsonGenerator.writeArrayFieldStart("roles");
        jsonGenerator.writeArray(roles,0, roles.length);
        jsonGenerator.writeEndArray();

        jsonGenerator.writeEndObject();


    }
}
