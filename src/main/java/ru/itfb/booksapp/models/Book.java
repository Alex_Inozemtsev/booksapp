package ru.itfb.booksapp.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter

@Entity
@Table(name = "books")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    String name;
    String description;


    @ManyToMany(mappedBy = "books")
    Set<Author> authors;

    public  Book(Long id, String name, String description){
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Book() {

    }

}
