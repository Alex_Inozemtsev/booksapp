package ru.itfb.booksapp.dtos;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPostDto {

    private Long id;

    private String username;

    private String password;

    private Boolean isAdmin = false;
}
