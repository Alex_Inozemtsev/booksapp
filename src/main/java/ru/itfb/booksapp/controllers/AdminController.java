package ru.itfb.booksapp.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itfb.booksapp.dtos.UserPostDto;
import ru.itfb.booksapp.mappers.UserMapper;
import ru.itfb.booksapp.mappers.UserMapperImpl;
import ru.itfb.booksapp.models.Role;
import ru.itfb.booksapp.models.User;
import ru.itfb.booksapp.services.UserService;

import java.util.HashSet;

@RestController
public class AdminController {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    UserService userService;

    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Iterable<User>> getAllUsers() {
        return new ResponseEntity(userService.allUsers(), HttpStatus.OK);
    }

    @PostMapping(value = "/user", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> addUser( UserPostDto user) {

        userService.saveOrUpdateUser(userMapper.UserPostToUser(user));

        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
