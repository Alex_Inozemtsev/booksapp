package ru.itfb.booksapp.controllers;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.itfb.booksapp.exeptions.ErrorDetails;
import ru.itfb.booksapp.exeptions.ItemNotFoundException;

@RestControllerAdvice
public class ExceptionController {

    @ExceptionHandler(ItemNotFoundException.class)
    public ResponseEntity<?> handleItemException(
            ItemNotFoundException ex
    ){
        var details = new ErrorDetails();
        details.setTitle("Resource not found");
        details.setStatus(HttpStatus.NOT_FOUND.value());
        details.setDetail(ex.getMessage());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }
}
