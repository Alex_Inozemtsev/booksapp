package ru.itfb.booksapp.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.itfb.booksapp.services.AuthorService;
import ru.itfb.booksapp.services.BookService;


@RestController
public class UserController {

    @Autowired
    BookService bookService;

    @Autowired
    AuthorService authorService;

    @GetMapping(value = "/findbooks", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Iterable<String>> findBooks(@RequestParam("author_like") String authorName) {
        return new ResponseEntity<Iterable<String>>(bookService.getBookNamesByAuthor(authorName), HttpStatus.OK);
    }

    @PostMapping(value = "/book", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> saveBook(
            @RequestParam Long id,
            @RequestParam String name,
            @RequestParam String description
    ) {
        bookService.saveOrUpdateBook(id,name,description);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/author", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> saveAuthor(
            @RequestParam Long id,
            @RequestParam String name,
            @RequestParam String description
    ) {

        authorService.saveOrUpdateAuthor(id,name,description);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
