package ru.itfb.booksapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


import ru.itfb.booksapp.models.Author;
import ru.itfb.booksapp.models.Book;

import ru.itfb.booksapp.services.AuthorService;
import ru.itfb.booksapp.services.BookService;
import ru.itfb.booksapp.services.UserService;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
public class UnauthorizedController {


    @Autowired
    BookService bookService;

    @Autowired
    AuthorService authorService;

    @Autowired
    UserService us;

    @GetMapping(value = "/books", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Book> getAllBooks() {
        return bookService.allBooks();
    }

    @GetMapping(value = "/authors", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Author> getAllAuthors() {
        return authorService.allAuthors();
    }

    @GetMapping(value = "/author/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String,String> getAuthorDescription(@PathVariable Long id) {
        return Collections.singletonMap("description", authorService.getDescriptionById(id));
    }

    @GetMapping(value = "/book/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String,String> getBookDescription(@PathVariable Long id) {
        return Collections.singletonMap("description", bookService.getDescriptionById(id));
    }

    @GetMapping(value = "/about", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String,Object> userInfo(Authentication authentication) {
        Map<String, Object> responseJSON = new HashMap<>();
        if (authentication != null) {
            String username = authentication.getName();

            responseJSON.put("isAuthenticated", true);
            responseJSON.put("username", username);
            responseJSON.put(
                    "role",
                    us.loadUserByUsername(username).getAuthorities().stream()
                            .map(GrantedAuthority::getAuthority).toList());
            return responseJSON;
        }

        responseJSON.put("isAuthenticated", false);
        responseJSON.put("username", null);
        responseJSON.put("role", null);
        return responseJSON;
    }


}
