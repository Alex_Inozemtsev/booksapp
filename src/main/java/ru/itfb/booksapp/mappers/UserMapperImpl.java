package ru.itfb.booksapp.mappers;

import org.springframework.stereotype.Component;
import ru.itfb.booksapp.dtos.UserPostDto;
import ru.itfb.booksapp.models.Role;
import ru.itfb.booksapp.models.User;

import java.util.HashSet;


@Component
public class UserMapperImpl implements UserMapper {
    @Override
    public User UserPostToUser(UserPostDto dto) {
        var roles = new HashSet<Role>();
        roles.add(new Role(1L, "ROLE_USER"));
        if (dto.getIsAdmin())
            roles.add(new Role(0L, "ROLE_ADMIN"));

        return new User(
                dto.getId(),
                dto.getUsername(),
                dto.getPassword(),
                roles
            );
    }
}
