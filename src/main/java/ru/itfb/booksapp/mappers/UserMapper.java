package ru.itfb.booksapp.mappers;


import org.mapstruct.Mapper;
import ru.itfb.booksapp.dtos.UserPostDto;
import ru.itfb.booksapp.models.User;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User UserPostToUser(UserPostDto dto);
}
