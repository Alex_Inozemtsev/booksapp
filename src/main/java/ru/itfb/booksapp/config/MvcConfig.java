package ru.itfb.booksapp.config;



import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/edit-users").setViewName("edit-users");
        registry.addViewController("/edit-books").setViewName("edit-books");
        registry.addViewController("/edit-authors").setViewName("edit-authors");
        registry.addViewController("/").setViewName("index");
    }




}
