package ru.itfb.booksapp.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itfb.booksapp.exeptions.ItemNotFoundException;
import ru.itfb.booksapp.models.Author;
import ru.itfb.booksapp.repositories.AuthorsRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorService {

    @Autowired
    AuthorsRepository authorsRepository;

    public List<Author> allAuthors() {
        return authorsRepository.findAll();
    }

    public String getDescriptionById(Long id) {
        Author author =
                authorsRepository
                        .findById(id)
                        .orElseThrow(() -> new ItemNotFoundException("Author with id " + id + "not found"));
        return author.getDescription();
    }

    @Transactional
    public void saveOrUpdateAuthor(Long id, String name, String description) {
        var givenAuthor = new Author(id, name, description);

        Optional<Author> existingAuthor = authorsRepository.findById(id);

        if (existingAuthor.isPresent()) {
            existingAuthor.get().setName(givenAuthor.getName());
            existingAuthor.get().setDescription(givenAuthor.getDescription());
        } else {
            authorsRepository.save(givenAuthor);
        }
    }


}
