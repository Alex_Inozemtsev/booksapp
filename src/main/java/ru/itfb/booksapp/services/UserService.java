package ru.itfb.booksapp.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itfb.booksapp.models.Role;
import ru.itfb.booksapp.models.User;
import ru.itfb.booksapp.repositories.RolesRepository;
import ru.itfb.booksapp.repositories.UsersRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    UsersRepository userRep;

    @Autowired
    RolesRepository roleRep;

    @Autowired
    PasswordEncoder noEncode;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRep.findByUsername(s);

        if (user == null) {
            throw new UsernameNotFoundException("User" + s + "not found");
        }

        return user;
    }

    @Transactional
    public boolean saveOrUpdateUser(User givenUser) {
        Optional<User> existingUser = userRep.findById(givenUser.getId());

        if (existingUser.isPresent()){
            existingUser.get().setUsername(givenUser.getUsername());
            existingUser.get().setPassword(givenUser.getPassword());
            existingUser.get().setRoles(givenUser.getRoles());
            return false;
        }

        userRep.save(givenUser);
        return true;
    }


    public boolean deleteUser(Long id) {
        if (userRep.findById(id).isPresent()) {
            userRep.deleteById(id);
            return true;
        }
        return false;
    }

    public List<User> allUsers() {
        return userRep.findAll();
    }
}

