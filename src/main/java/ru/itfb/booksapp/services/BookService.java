package ru.itfb.booksapp.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itfb.booksapp.exeptions.ItemNotFoundException;
import ru.itfb.booksapp.models.Author;
import ru.itfb.booksapp.models.Book;
import ru.itfb.booksapp.models.User;
import ru.itfb.booksapp.repositories.BooksRepository;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {
    @Autowired
    BooksRepository booksRepository;

    public List<Book> allBooks() {
        return booksRepository.findAll();
    }

    public String getDescriptionById(Long id) {
        Book book =
                booksRepository
                        .findById(id)
                        .orElseThrow(() -> new ItemNotFoundException("Book with id " + id + "not found"));

        return book.getDescription();
    }

    public Iterable<String> getBookNamesByAuthor(String authorName) {
        return booksRepository.findBookByAuthor(authorName);
    }

    @Transactional
    public void saveOrUpdateBook(Long id, String name, String description){
        var givenBook = new Book(id, name, description);

        Optional<Book> existingBook = booksRepository.findById(id);

        if (existingBook.isPresent()) {
            existingBook.get().setName(givenBook.getName());
            existingBook.get().setDescription(givenBook.getDescription());
        } else {
            booksRepository.save(givenBook);
        }
    }

}
