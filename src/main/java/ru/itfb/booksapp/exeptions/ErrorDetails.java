package ru.itfb.booksapp.exeptions;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class ErrorDetails {
    private String title;
    private int status;
    private String detail;
}
