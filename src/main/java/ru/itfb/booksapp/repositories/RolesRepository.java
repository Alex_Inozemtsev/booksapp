package ru.itfb.booksapp.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itfb.booksapp.models.Role;

@Repository
public interface RolesRepository extends JpaRepository<Role, Long> {
}
