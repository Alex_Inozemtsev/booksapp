package ru.itfb.booksapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
import ru.itfb.booksapp.models.Author;

@Repository
public interface AuthorsRepository extends JpaRepository<Author, Long> {

}
