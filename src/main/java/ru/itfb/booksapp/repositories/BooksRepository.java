package ru.itfb.booksapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.itfb.booksapp.models.Book;

@Repository
public interface BooksRepository extends JpaRepository<Book, Long> {

    @Query(value = """
                        SELECT b.name
                        FROM books AS b
                        INNER JOIN author_book AS a_b
                            ON b.id = a_b.book_id
                        INNER JOIN authors AS a
                            ON a.id = a_b.author_id
                        WHERE a.name LIKE '%' || :author_like || '%'
                            
            """,
            nativeQuery = true)
    Iterable<String> findBookByAuthor(@Param("author_like") String author);
}
